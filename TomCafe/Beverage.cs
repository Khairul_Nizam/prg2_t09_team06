﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomCafe
{
    class Beverage : Product
    {
        private double tradeIn;

        public double TradeIn
        {
            get { return tradeIn; }
            set { tradeIn = value; }
        }
        public Beverage():base() { }
        public Beverage(string n, double p, double t) : base(n, p)
        {
            tradeIn = t;
        }
        public override double GetPrice()
        {
            return 0.0;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
