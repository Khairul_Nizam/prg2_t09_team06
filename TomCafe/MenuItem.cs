﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomCafe
{
    class MenuItem
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private double price;

        public double Price
        {
            get { return price; }
            set { price = value; }
        }
        private List<Product> productList = new List<Product>();

        public List<Product> ProductList
        {
            get { return productList; }
            set { productList = value; }
        }
        public MenuItem() { }
        public MenuItem(string n, double p)
        {
            name = n;
            price = p;
        }
        public double GetTotalPrice()
        {
            return 0.0;
        }
        public override string ToString()
        {
            return string.Format("{0}\n{1}", name, price);
        }

    }
}
