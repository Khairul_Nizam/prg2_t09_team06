﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TomCafe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<Product> productList = new List<Product>();
        List<MenuItem> beverageList = new List<MenuItem>();
        List<MenuItem> valueMealList = new List<MenuItem>();
        List<MenuItem> bundleMealList = new List<MenuItem>();
        List<MenuItem> sidesList = new List<MenuItem>();
        List<MenuItem> itemList = new List<MenuItem>();
        List<MenuItem> cartList = new List<MenuItem>();

        List<ValueMeal> List = new List<ValueMeal>();

        public MainPage()
        {
            this.InitializeComponent();

            DateTime d = DateTime.Now;
            DateTime d1 = new DateTime(d.Year, d.Month, d.Day, 7, 0, 0);
            DateTime d2 = new DateTime(d.Year, d.Month, d.Day, 14, 0, 0);
            DateTime d3 = new DateTime(d.Year, d.Month, d.Day, 10, 0, 0);
            DateTime d4 = new DateTime(d.Year, d.Month, d.Day, 19, 0, 0);
            DateTime d5 = new DateTime(d.Year, d.Month, d.Day, 16, 0, 0);
            DateTime d6 = new DateTime(d.Year, d.Month, d.Day, 22, 0, 0);
            DateTime d7 = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
            DateTime d8 = new DateTime(d.Year, d.Month, d.Day + 1, 0, 0, 0);

            MenuItem m1 = new MenuItem("Breakfast set", 7.90);
            m1.ProductList.Add(new ValueMeal("Hotcake with Sausage", 6.90, d1, d2));
            m1.ProductList.Add(new Sides("Hash brown", 2.10));
            bundleMealList.Add(m1);

            MenuItem m2 = new MenuItem("Hamburger Combo", 10.20);
            m2.ProductList.Add(new ValueMeal("Hamburger", 7.50, d3, d4));
            m2.ProductList.Add(new Sides("Truffle Fries", 3.70));
            m2.ProductList.Add(new Beverage("Cola", 2.85, 0.0));
            bundleMealList.Add(m2);

            MenuItem m3 = new MenuItem("Dinner Set", 18.50);
            m3.ProductList.Add(new ValueMeal("Ribeye steak", 10.20, d5, d6));
            m3.ProductList.Add(new Sides("Truffle Fries", 3.70));
            m3.ProductList.Add(new Sides("Caesar salad", 4.30));
            m3.ProductList.Add(new Beverage("Coffee", 2.70, 0.0));
            bundleMealList.Add(m3);

            valueMealList.Add(new MenuItem("Hotcake with Sausage", 6.90));
            valueMealList.Add(new MenuItem("Hamburger", 7.50));
            valueMealList.Add(new MenuItem("Ribeye Steak", 10.20));
            valueMealList.Add(new MenuItem("Nasi Lemak", 5.40));

            sidesList.Add(new MenuItem("Hash brown", 2.10));
            sidesList.Add(new MenuItem("Truffle fries", 3.70));
            sidesList.Add(new MenuItem("Calamari", 3.40));
            sidesList.Add(new MenuItem("Caesar salad", 4.30));


            beverageList.Add(new MenuItem("Cola", 2.85));
            beverageList.Add(new MenuItem("Green Tea", 3.70));
            beverageList.Add(new MenuItem("Coffee", 2.70));
            beverageList.Add(new MenuItem("Tea", 2.70));
            beverageList.Add(new MenuItem("Tom's Root Beer", 9.70));
            beverageList.Add(new MenuItem("Mocktail", 15.90));            
        }

        private void mainsButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime d = DateTime.Now;
            DateTime d1 = new DateTime(d.Year, d.Month, d.Day, 7, 0, 0);
            DateTime d2 = new DateTime(d.Year, d.Month, d.Day, 14, 0, 0);
            DateTime d3 = new DateTime(d.Year, d.Month, d.Day, 10, 0, 0);
            DateTime d4 = new DateTime(d.Year, d.Month, d.Day, 19, 0, 0);
            DateTime d5 = new DateTime(d.Year, d.Month, d.Day, 16, 0, 0);
            DateTime d6 = new DateTime(d.Year, d.Month, d.Day, 22, 0, 0);
            DateTime d7 = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
            DateTime d8 = new DateTime(d.Year, d.Month, d.Day + 1, 0, 0, 0);

            List<ValueMeal> tempList = new List<ValueMeal>();
            ValueMeal v1 = new ValueMeal("Hotcake with Sausage", 6.90, d1, d2);
            ValueMeal v2 = new ValueMeal("Hamburger", 7.50, d3, d4);
            ValueMeal v3 = new ValueMeal("Ribeye steak", 10.20, d5, d6);
            ValueMeal v4 = new ValueMeal("Nasi Lemak", 5.40, d7, d8);

            /* if (DateTime.Now.Hour >= v1.StartTime.Hour && DateTime.Now.Hour < v2.StartTime.Hour)
            {
                tempList.Add(v1);
                tempList.Add(v4);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.Hour >= v2.StartTime.Hour && DateTime.Now.Hour < v1.EndTime.Hour)
            {
                tempList.Add(v1);
                tempList.Add(v2);
                tempList.Add(v4);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.Hour >= v1.EndTime.Hour && DateTime.Now.Hour < v3.StartTime.Hour)
            {
                tempList.Add(v2);
                tempList.Add(v4);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.Hour >= v3.StartTime.Hour && DateTime.Now.Hour < v2.EndTime.Hour)
            {
                tempList.Add(v2);
                tempList.Add(v3);
                tempList.Add(v4);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.Hour >= v2.EndTime.Hour && DateTime.Now.Hour < v3.EndTime.Hour)
            {
                tempList.Add(v3);
                tempList.Add(v4);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else
            {
                tempList.Add(v4);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }*/

            if (v1.IsAvailable() == true)
            {
                tempList.Add(v1);
            }
            else if (v2.IsAvailable() == true)
            {
                tempList.Add(v2);
            }
            else if (v3.IsAvailable() == true)
            {
                tempList.Add(v3);
            }
            else
            {
                tempList.Add(v4);
            }
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = tempList;
        }

        private void sidesButton_Click(object sender, RoutedEventArgs e)
        {
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = sidesList;
        }

        private void beveragesButton_Click(object sender, RoutedEventArgs e)
        {
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = beverageList;
        }

        private void bundlesButton_Click(object sender, RoutedEventArgs e)
        {
            /* TimeSpan a = new TimeSpan(7, 00, 00);
            TimeSpan b = new TimeSpan(10, 00, 00);
            TimeSpan c = new TimeSpan(14, 00, 00);
            TimeSpan d = new TimeSpan(16, 00, 00);
            TimeSpan f = new TimeSpan(19, 00, 00);
            TimeSpan g = new TimeSpan(22, 00, 00);

            MenuItem m1 = new MenuItem("Breakfast set", 7.90);
            MenuItem m2 = new MenuItem("Hamburger Combo", 10.20);
            MenuItem m3 = new MenuItem("Dinner Set", 18.50);

            List<MenuItem> tempList = new List<MenuItem>();
            if (DateTime.Now.TimeOfDay >= a && DateTime.Now.TimeOfDay < b)
            {
                tempList.Add(m1);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.TimeOfDay >= b && DateTime.Now.TimeOfDay < c)
            {
                tempList.Add(m1);
                tempList.Add(m2);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.TimeOfDay >= c && DateTime.Now.TimeOfDay < d)
            {
                tempList.Add(m2);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.TimeOfDay >= d && DateTime.Now.TimeOfDay < f)
            {
                tempList.Add(m2);
                tempList.Add(m3);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            }
            else if (DateTime.Now.TimeOfDay >= f && DateTime.Now.TimeOfDay < g)
            {
                tempList.Add(m3);
                itemsListView.ItemsSource = null;
                itemsListView.ItemsSource = tempList;
            } */
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void orderButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
 
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
